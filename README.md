node-nsca
=========
Submit passive check results and performance data to nagios through nsca.

installation
============
npm i nsca --save

usage
=====

```js
NSCA = require('nsca');

var nsca = new NSCA({
  port: 5667,             // port of NSCA agent
  host: '127.0.0.1',      // hostname of NSCA agent
  serviceHost: 'fooHost',    // this service's hostname.
  serviceName: 'fooService'
});

/**
 * submit a warning
 */
nsca.submit(
  NSCA.RESULT_WARNING,
  'service (re)started'
);

/**
 * Submit performance data according to http://nagiosplug.sourceforge.net/developer-guidelines.html#AEN200
 */
nsca.submit(
  NSCA.RESULT_OK,
  'Performance data', // (optional) label, default: undefined
   42,                // (optional) value, default: undefined
   NSCA.UNIT_COUNTER, // (optional) measurement unit indicator, default: undefined
);
```

constants
=========
Return codes according to http://nagiosplug.sourceforge.net/developer-guidelines.html#AEN78:

```js
NSCA.RESULT_OK =       '0';
NSCA.RESULT_WARNING =  '1';
NSCA.RESULT_CRITICAL = '2';
NSCA.RESULT_UNKNOWN =  '3';

```

Units according to http://nagiosplug.sourceforge.net/developer-guidelines.html#AEN200

```js
NSCA.UNIT_NUMBER = undefined // this is the default
NSCA.UNIT_SECONDS = 's';
NSCA.UNIT_PERCENT = '%';
NSCA.UNIT_BYTES =   'B';
NSCA.UNIT_KBYTES =  'KB';
NSCA.UNIT_MBYTES =  'MB';
NSCA.UNIT_COUNTER = 'c';
```
