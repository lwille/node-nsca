var net = require('net'),
events = require('events'),
util = require('util');

module.exports = function() {

  var NSCA = function(options) {
    this.options = options;
    events.EventEmitter.call(this);
  };
  util.inherits(NSCA, events.EventEmitter);

  NSCA.prototype.submit = function(nagiosCode, label, value, unit, cb) {
    var data = [
      this.options.serviceHost,
      this.options.serviceName,
      nagiosCode,
      [label, '=', value.toString(), (unit ? unit : '')].join('')
    ].join('|');

    conn = net.connect(this.options, function() {
      conn.write(data, conn.end.bind(conn));
    });

    if (cb) {
      conn.on('error', cb.bind(null));
      conn.on('end', cb.bind(null, null));
    } else {
      conn.on('error', function(){}); // silence connection errors
    }
  };


  return NSCA;
}();
